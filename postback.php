<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Neverblue Update</title>

<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.min.js"></script>

</head>

<body style="text-align: center; margin: 0 auto; background: #999999;">
<div align="center" >
<form name="postback" id="postback" method="post" action="process.php">
<table border="0" cellpadding="0" cellspacing="0" width="1000" style="background: #999999; width: 1000px; border: none; font-family:'Arial Black', Gadget, sans-serif;" align="center">
	<tr>
		<td width="160">&nbsp;</td>
		<td width="40">&nbsp;</td>
		<td width="800">&nbsp;</td>
	</tr>
	<tr>
		<td width="160" align="right">
			<label for="campaignId">Campaign Id</label>
		</td>
		<td width="40">&nbsp;</td>
		<td width="800">
			<input type="text" id="campaignId" name ="campaignId" size="80"/>
		</td>
	</tr>
	<tr>
		<td width="160" align="right">
			<label for="url">url</label>
		</td>
		<td width="40">&nbsp;</td>
		<td width="800">
			<input type="text" id="url" name ="url" size="80"/>
		</td>
	</tr>
	<tr>
		<td width="160" align="right">
			<label for="clicks">List of Clicks</label>
		</td>
		<td width="40">&nbsp;</td>
		<td width="800">
			<textarea rows="18" cols="90" id="clicks" name ="clicks"></textarea>
		</td>
	</tr>
</table>
<button type="submit" id="submit">submit</button>
</form>

<table border="0" cellpadding="0" cellspacing="0" width="1000" style="background: #999999; width: 600px; border: none; font-family:'Arial Black', Gadget, sans-serif;" align="center">
	<tr>
		<td width="600" id="responce">
			
		</td>
	</tr>
</table>
	
</div>
<script type="text/javascript">
	
$(document).ready( function() {
	$("#submit").click(function(e) {
		
		e.preventDefault();
		
		var script = $("#clicks").val().replace(/(\r\n|\n|\r)/gm,"");
		
		var clicks =  script.split(",");

		timeAjax(clicks, 0, clicks.length, 2000);
		
		
//		for(var i = 0; i < clicks.length; i++) {
//			
//			var val = clicks[i].split(":");
//			
//			var clickId = val[0];
//			var amount = "";
//			
//			if (val.length > 1) {
//				amount = val[1];
//			}
//			
//			
//			$.ajax({
//				url: "process.php",
//				type: "get",
//				data: {url: $("#url").val(), 
//						clickId: clickId,
//						amount: amount},
//				success: function (html) {
//					$("#responce").append(html);
//				}
//			});
//		}
		
	});
});

function timeAjax(clicks, i, total, time)
{
	var val = clicks[i].split(":");
			
	var clickId = val[0];
	var amount = "";

	if (val.length > 1) {
		amount = val[1];
	}

	setTimeout(function() {
		$.ajax({
			url: "process.php",
			type: "get",
			data: {url: $("#url").val(), 
					clickId: clickId,
					amount: amount},
			success: function (html) {
				$("#responce").append(html);
				
				i++;

				if (i < total) {
					timeAjax(clicks, i, total, time);
				} else {
					$("#responce").append($("<div>").html("Process finished."));
				}
			}
		});
	}, time);
	
}

</script>
	
	
</body>
	
</html>