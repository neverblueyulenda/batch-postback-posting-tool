<?php
    $mapped_advertisers = array('groupon', 'taobao', 'cpaevents', 'apple', 'ndp');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Batch Post Rev Share Conversions Tool</title>
    <style media="screen" type="text/css">
        img.help:hover {cursor: hand;}
        body { text-align: center; margin: 0 auto; background: #cccccc; }
        table { background: #cccccc; width: 1000px; border: none; font-family:'Arial Black', Gadget, sans-serif; }
        .notes { color:#aaaaaa; font-style:italic }
    </style>
</head>

<body>
<h1>Batch Post Rev Share Conversions</h1>    
<p>posts conversion to the Event Collector</p>
<div align="center" >
<form name="postback" id="postback" method="post" action="ec-process.php">
    <table border="0" cellpadding="0" cellspacing="0" width="auto" align="center">
        <tr>
            <td width="160">&nbsp;</td>
            <td width="40">&nbsp;</td>
            <td width="auto">&nbsp;</td>
        </tr>
        <tr>
            <td width="160" align="right">
                <label for="advertiser">Select the Advertiser</label>
            </td>
            <td width="40">&nbsp;</td>
            <td width="auto">
                <select name="advertiser" id="advertiser">
                    <option value="">--</option>
                    <?php
                        foreach ($mapped_advertisers as $adv) {
                            if ($_REQUEST['advertiser'] == $adv) {
                                $selected = ' selected';
                            } else {
                                $selected = '';
                            }
                            echo('<option value="'.$adv.'"'.$selected.'>'.ucfirst($adv).'</option>');
                        }
                    ?>
                </select> 
            </td>
        </tr>
        <tr>
            <td width="160" align="right">
                <label for="conversion_data">List of Conversions</label>
            </td>
            <td width="40">&nbsp;</td>
            <td width="auto">
                cps: CLICKID,ORDER_AMT,ORDERID,COMMISSION_FEE&nbsp;<img class="help" src="icon-help.png" title="CLICKID must be in reqid__oid format."><br/>
                <span class="notes">Accepted permutations:<br />
                    1234587__23211,12.33,,<br />                    
                    1234587__23211,12.33,201506051732,<br />
                    1234587__23211,12.33,,1.23<br />
                    1234587__23211,12.33,CURRENT_TIMESTAMP,1.23 <img class="help" src="icon-help.png" title="Will use current timestamp as the orderid"><br />
                    1234587__23211,12.33,201506051732,1.23<br />
                    1234587__23211,event-name</span><br />
                cpa: CLICKID,EVENT_NAME&nbsp;<img class="help" src="icon-help.png" title="CLICKID must be in reqid__oid format. 
EVENT_NAME must be without spaces. eg: new_users_first_booking"><br/>
                <span class="notes">Accepted permutations:<br />
                    1234587__23211,manual_lead</span><br />
                cpi: CLICKID&nbsp;<img class="help" src="icon-help.png" title="CLICKID must be in reqid__oid format. 
EVENT_NAME must be without spaces. eg: new_users_first_booking"><br/>
                <span class="notes">Accepted permutations:<br />
                    1234587__23211</span><br />
                <textarea rows="25" cols="110" id="conversion_data" name="conversion_data"><?php echo($_REQUEST['conversion_data']);?></textarea>
            </td>
        </tr>
    </table>
    <button type="submit" id="submit">submit</button>
</form>
</body>
</html>    
