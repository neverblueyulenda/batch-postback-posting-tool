<?php
date_default_timezone_set("America/Los_Angeles");
$advertiser = $_REQUEST['advertiser'];
$conversion_data = $_REQUEST['conversion_data'];

$tmp = explode('/',$_SERVER['SCRIPT_NAME']);
$process_root = '/'.$tmp[1];
$home = 'http://'.$_SERVER['SERVER_NAME'].$process_root;

if (!$_REQUEST) {
    header('Location: '.$home);
    die();
}

if (!$advertiser) {
    echo('You did not select the advertiser!<br /><a href="javascript:history.go(-1)">Go back</a>');
    die();
}

if (!$conversion_data) {
    echo('You did not enter any conversion data!<br /><a href="javascript:history.go(-1)">Go back</a>');
    die();
}

$base_ec_url = 'https://s2s.apinbm.com/nbrvs/'.$advertiser.'?';
if ($advertiser == 'cpaevents' || $advertiser == 'apple' || $advertiser == 'ndp') {
    $base_ec_url = 'https://s2s.apinbm.com/'.$advertiser.'?';    
    //$base_ec_url = 'http://ec-dev.nbinternal.com:8888/'.$advertiser.'?';    // dev url
}

$errors = array();
$sent = array();

$i = 0;
foreach (preg_split("/((\r?\n)|(\r\n?))/", $conversion_data) as $conversion){
    $i++;
    $bad_data_reason = '';
    $data = explode(',', $conversion);
    /*
        $data[0] = clickid
        $data[1] = order_amt || $data[1] = event_name
        $data[2] = orderid
        $data[3] = commission_fee
    */
    if (!$data[0] || !strpos($data[0], '__')) {
        $bad_data_reason .= 'invalid clickid; ';        
    }
    if ($advertiser == 'cpaevents') {
        if (!$data[1]) {
            $bad_data_reason .= 'no event name specified; ';
        } else {
            $url = $base_ec_url.'clickid='.$data[0].'&event='.strtolower($data[1]);
        }
    } elseif ($advertiser == 'ndp') {
        $url = $base_ec_url.'clickid='.$data[0];
    } elseif ($advertiser == 'apple') {
	if (!$data[1]) {
	   $data[1] = 'lead';
	}
        $url = $base_ec_url.'clickid='.$data[0].'&event='.strtolower($data[1]);
    } else {
        if (!$data[1] || !is_numeric($data[1])) {
            $bad_data_reason .= 'invalid order_amt; ';
        }
        if (sizeof($data) != 4) {
            $bad_data_reason .= 'missing some parameter; ';
        }    
        if ($advertiser == 'taobao' && (!$data[3] || !is_numeric($data[3]))) {
            $bad_data_reason = 'taobao requires commission_fee';
        }

        if (!$data[2] || $data[2] == 'CURRENT_TIMESTAMP') {
            // set orderid = current_timestamp-$i
            $data[2] = time().'-'.$i;
        }
        $url = $base_ec_url.'a=1&clickid='.$data[0].'&order_amt='.$data[1].'&orderid='.$data[2];
        if ($data[3]) {
            $url .= '&commission_fee='.$data[3];
        }
    }
    
    if ($bad_data_reason) {
        $errors[] = $conversion.' <-- '.$bad_data_reason;
        continue;
    }
    
    $response = post_conversion($url);    
    $sent[$response][] = $url;
}

print_results($sent, $errors, $process_root);

function print_results($sent, $errors, $process_root) {
    echo('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">');
    echo('<html xmlns="http://www.w3.org/1999/xhtml">');
    echo('<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>Batch Post Rev Share Conversions Tool</title></head>');
    echo('<body>');
    echo('<h2>Results</h2>');
    echo('<p><a href="'.$process_root.'">Post more conversions</a></p>');    
    if (sizeof($errors) > 0 || isset($sent[0])) {
        echo('<p style="color:red">Failed conversions: <br /><textarea rows="15" cols="110">');
        if (sizeof($errors) > 0) { 
            foreach ($errors as $failed) {
                echo($failed.PHP_EOL);
            }
        }
        if (isset($sent[0]) && sizeof($sent[0]) > 0) {
            foreach ($sent[0] as $failed_ec) {
                echo($failed_ec.PHP_EOL);
            }
        }
        echo('</textarea></p><hr />');
    }
    if (isset($sent[1]) && sizeof($sent[1]) > 0) {
        echo('<p style="color:green">Succeed conversions: <br /><textarea rows="15" cols="110">');    
        foreach ($sent[1] as $success) {
            echo($success.PHP_EOL);
        }
        echo('</textarea></p>');
    }
    echo('</body></html>');
        
    return;
}

function post_conversion($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $output = curl_exec($ch);
    curl_close($ch);
    
    return $output;
}
?>
