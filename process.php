<?php

class StringIO
{
    /**
     * Construct a StringIO object.
     */
    public function __construct()
    {
        $this->contents = '';
    }

    /**
     * Add data to contents
     *
     * @param resource $ch Curl Resource Handler (unused)
     * @param string $data Data to add to contents
     *
     * @return int
     */
    public function write($ch, $data)
    {
        $this->contents .= $data;

        return strlen($data);
    }

    /**
     * Retrieve current contents
     *
     * @return string
     */
    public function contents()
    {
        return $this->contents;
    }
}

function parseHttpHeaders($headers)
{
	$retVal = array();
	$fields = explode("\r\n", preg_replace('/\x0D\x0A[\x09\x20]+/', ' ', $headers));
	foreach ($fields as $field) {
		if (preg_match('/([^:]+): (.+)/m', $field, $match)) {
			$match[1] = preg_replace('/(?<=^|[\x09\x20\x2D])./e', 'strtoupper("\0")', strtolower(trim($match[1])));
			if (isset($retVal[$match[1]])) {
				$retVal[$match[1]] = array($retVal[$match[1]], $match[2]);
			} else {
				$retVal[$match[1]] = trim($match[2]);
			}
		}
	}
	return $retVal;
}



$data = $_REQUEST;

$campaignId = $data['campaignId'];
$url = $data['url'];
//$clicks = explode(",", $data['clicks']);

$click = $data["clickId"];
$amount = $data["amount"];

$response;
$postbackUrl;

if (strlen($amount) === 0 && strpos($url, "SALE_AMOUNT") !== false) {
	print_r("<pre>");
	$response = $click . ":  <span style='color: red;'>Notice!</span> The amount value key exists in the postback url but is missing amount from the list<br/>";
	
	print_r($response);
	die;
} else if (strlen($amount) > 0 && strpos($url, "SALE_AMOUNT") === false) {
	print_r("<pre>");
	$response = $click . ":  <span style='color: red;'>Notice!</span> You are adding sale amount, but the \"SALE_AMOUNT\" key does not exit in the postback url<br/>";
	
	print_r($response);
	die;
} else if (strlen($amount) > 0 && !is_numeric($amount)) {
	print_r("<pre>");
	$response = $click . ":  <span style='color: red;'>Notice!</span> Please check the amount value: <span style='color: red;'>$amount</span><br/>";
	
	print_r($response);
	die;
} else {
	$postbackUrl = str_replace("SALE_AMOUNT", $amount, $url);
}

//foreach ($clicks as $click) {
	
	$postbackUrl = str_replace("REQUEST_ID", $click, $postbackUrl);

//	$ch = curl_init();
//	curl_setopt($ch, CURLOPT_URL, $postbackUrl);
//	curl_setopt($ch, CURLOPT_HTTPGET, 1);
//	
//	$response_headers_io = new StringIO();
//	curl_setopt($ch, CURLOPT_HEADERFUNCTION, array(&$response_headers_io, 'write'));
//
//	# Capture the response body...
//	$response_body_io = new StringIO();
//	curl_setopt($ch, CURLOPT_WRITEFUNCTION, array(&$response_body_io, 'write'));
	
	try {
//		# Run the request.
//		curl_exec($ch);
//		$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
//		curl_close($ch);
//
//		# Get the headers...
//		$parsed_headers = parseHttpHeaders($response_headers_io->contents());
//		$response_headers = array("http_code" => $http_code);
//		foreach ($parsed_headers as $key => $value) {
//			$response_headers[strtolower($key)] = $value;
//		}
//
//		# Get the body...
//		$response_body = $response_body_io->contents();
//
//		if ($response_headers["http_code"] == '200') {
//			$response = $click . ": Okay<br/>";
//		} else {
//			$response = $click . ":  <span style='color: red;'>Notice!</span> " . $response_headers["http_code"] . "<br/>";
//		}
//		
//		$response = array($response_headers, $response_body);
//		
//		$response = array($response_headers, $response_body);
//		
		/**
		 * @internal the format seems changed to xml, lets use XML call
		 */
		$xmlString = simplexml_load_string(file_get_contents($postbackUrl));
		if ($xmlString->code == 1) {
			$response = $click . ": $xmlString->msg";
		} else {
			$response = $click . ":  <span style='color: red;'>Notice!</span> " . $xmlString->msg . "<br/>";
		}
		
	} catch (Exception $e) {
		curl_close($ch);
		error_log('Error: ' . $e->getMessage());
		
		$response = 'Error: ' . $e->getMessage();
	}
//}


print_r("<pre>");
print_r($response);